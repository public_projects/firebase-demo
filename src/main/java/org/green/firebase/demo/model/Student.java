package org.green.firebase.demo.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Student {
    private String id;
    private int age;
    private String gender;
    private String firstName;
    private String lastName;
}
