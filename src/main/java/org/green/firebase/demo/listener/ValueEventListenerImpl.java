package org.green.firebase.demo.listener;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public class ValueEventListenerImpl implements ValueEventListener {
    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {
        log.info("all values {}", dataSnapshot.getValue());
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {

    }
}
