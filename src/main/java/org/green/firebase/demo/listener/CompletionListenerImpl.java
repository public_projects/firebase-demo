package org.green.firebase.demo.listener;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CompletionListenerImpl implements DatabaseReference.CompletionListener {
    @Override
    public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
        log.info("Student saved!");
    }
}
