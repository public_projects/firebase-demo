package org.green.firebase.demo.service;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import org.green.firebase.demo.listener.ValueEventListenerImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RootService {
    private DatabaseReference rootDbReference;

    @Autowired
    public RootService(DatabaseReference rootDbReference){
        this.rootDbReference = rootDbReference;
    }

    public void getAll(){
        rootDbReference.addListenerForSingleValueEvent(new ValueEventListenerImpl());
    }
}
