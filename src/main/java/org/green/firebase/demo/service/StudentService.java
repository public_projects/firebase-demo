package org.green.firebase.demo.service;

import com.google.firebase.database.DatabaseReference;
import lombok.extern.slf4j.Slf4j;
import org.green.firebase.demo.listener.CompletionListenerImpl;
import org.green.firebase.demo.listener.ValueEventListenerImpl;
import org.green.firebase.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Slf4j
@Service
public class StudentService {

    private DatabaseReference studentDbReference;

    @Autowired
    public StudentService(DatabaseReference studentDbReference) {
        this.studentDbReference = studentDbReference;
    }

    public void getAll() {
        studentDbReference.addListenerForSingleValueEvent(new ValueEventListenerImpl());
    }

    public void add(Student student) {
        studentDbReference.child(student.getId()).setValue(student, new CompletionListenerImpl());
    }

    public void get(String id) {
        log.info("Fething student: {}", id);
        studentDbReference.child(id).addListenerForSingleValueEvent(new ValueEventListenerImpl());
    }

    public void update(Student student) {
        log.info("Updating student: {}", student.getId());
        Map<String, Object> updateRecord = new HashMap<>(1);
        updateRecord.put(student.getId(), student);
        studentDbReference.updateChildren(updateRecord, new CompletionListenerImpl());
    }
}
