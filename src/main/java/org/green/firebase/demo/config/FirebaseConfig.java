package org.green.firebase.demo.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;

@Slf4j
@Configuration
public class FirebaseConfig {

    @Bean
    public FirebaseApp getFirebaseApp() throws IOException {
        FileInputStream
                serviceAccount = new FileInputStream("D:\\software\\googleCredentials\\greenhorn-c7a65-firebase-adminsdk-x802d-9a5cc4a133.json");

        //Initialize the app with a service account, granting admin privileges
        FirebaseOptions options = new FirebaseOptions.Builder()
                                          .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                                          .setDatabaseUrl("https://greenhorn-c7a65.firebaseio.com")
                                          .build();
        return FirebaseApp.initializeApp(options);
    }

    @Bean
    public FirebaseDatabase getFirebaseDatabase(FirebaseApp firebaseApp){
        return FirebaseDatabase.getInstance(firebaseApp);
    }

    @Bean(name = "rootDbReference")
    public DatabaseReference getRootDbReference(FirebaseDatabase firebaseDatabase){
        DatabaseReference db = firebaseDatabase.getReference("dev_db_1");
        return firebaseDatabase.getReference("dev_db_1");
    }

    @Autowired
    @Bean(name = "studentDbReference")
    public DatabaseReference getStudentDbReference(DatabaseReference rootDbReference){
        return rootDbReference.child("student");
    }
}
