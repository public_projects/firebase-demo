package org.green.firebase.demo;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import lombok.extern.slf4j.Slf4j;
import org.green.firebase.demo.model.Student;
import org.green.firebase.demo.service.RootService;
import org.green.firebase.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Random;


@Slf4j
@SpringBootApplication
public class FirebaseDemoApplication implements CommandLineRunner {

    @Autowired
    private RootService rootService;

    @Autowired
    private StudentService studentService;

    public static void main(String[] args) {
        SpringApplication.run(FirebaseDemoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        // get root value
        rootService.getAll();
        // get all student
        studentService.getAll();
        Student student = getRandomStudent();
        // Adding new student
        studentService.add(student);
        // getting a specific student
        studentService.get(student.getId());
        Thread.sleep(10000);
        Student student2 = getRandomStudent();
        log.info("Updating student: {} lastname from {} to {}" , student.getId() , student.getLastName(), student2.getLastName());
        student.setLastName(student2.getLastName());
        // updating a specific student
        studentService.update(student);
    }

    public Student getRandomStudent() {
        // create instance of Random class
        Random rand = new Random();

        // Generate random integers in range 0 to 999
        int rand_int1 = rand.nextInt(1000);

        Student n = new Student();
        n.setAge(1);
        n.setGender("M");
        n.setId("G" + rand_int1);
        n.setLastName("R" + rand_int1);
        return n;
    }
}
